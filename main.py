import sys

def convert_to_array(array_string):
    array = array_string.strip("[]")
    array = array.split(",")
    array = [value.strip() for value in array]
    int_list = [int(value) for value in array]
    return int_list

if __name__ == "__main__":
    array_1 = convert_to_array(sys.argv[1])
    array_2 = convert_to_array(sys.argv[2])
    
    merged_array = list(set(array_1 + array_2))
    
    print(merged_array)
